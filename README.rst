
=======
 Usage
=======

 1. Download ant-salesforce.jar from Salesforce and copy it to the lib directory.

 2. Copy build.properties.template to build.properties and fill in the details.

 3. Running the targets:


    * On Unix::

	$ ./gradlew --info deploy



    * On Windows::

	C:\> gradlew.bat deploy


 * List of targets::

	deploy
	retrievek
	deployDryRun
	listMetadata
	describeMetadata
